[v8r4p7]
use DIRAC v6r16p3

* Pylint fix

[v8r4p6]
use DIRAC v6r16p3

* no longer necessary to add "/" at the end of the direcotry name
* change to simulation conditions
* add a BK update latency
* new script to just check file existence and availability
* fix doc string
* randomize list of targetSEs
* fix missing definition of bkQuery
* Allow run ranges (run1:run2)
* fix misleading logging
* correct tests
* add ganag tests LHCBDIRAC-453
* NagiosTopologyAgent upgrades for VAC and VCYCLE plus fixes in SE function (no more useles ERROR or WARN messages)
* Artificially boosting the number of events created for MC
* add more tests
* only files with descendants will be set as processed
* Setting files with descendants as processed
* add more integration tests: #LHCBDIRAC-453
* Setting files with descendants as processed
* add missing port
* add integration tests
* fix the stored procedures
* The configname and configversion of the run is also inserted to the productionscontainer table
* add configuration to the productions container

[v8r4p5]
use DIRAC v6r16p2

* Indentation fixed
* remove import
* WMSSecureGW to use ProxyManager
* Changed getName into a function
* correct the table schema
* Client tests upgrade
* if reqInfo does not exists in the requet, we have to use inform instead
* Removed raise_for_status()
* Continue to the execution of base class after error
* Fixed site name parsing

[v8r4p4]
use DIRAC v6r16p1

* minor fixes
* add temporary fix for pytools dependencies
* send a separate mail to the person who is in the request inform list
* Using 10 threads for performing BKK queries
* NTA fix
* Fixed database lock issue

[v8r4p3]
use DIRAC v6r16p1
* Change the import for suds module
* correct the dirac-install command

[v8r4p2]
use DIRAC v6r16
* Use LCgVersion 2016-11-03


[v8r4p1]
use DIRAC v6r16
* Use LCgVersion 2016-11-03

[v8r4]
use DIRAC v6r16
* change version for qt, pygraphics
* Continue execution
* Added elog credential check
* Re-added SLSAgent
* simple bug fix
* removed old test
* added missing agents
* Removed dependencies from SAM
* adding Monitoring to Jenkins
* Use LcgVer = 2016-10-24
* Update projectConfig.json
* Version without CMT
* unecessary files removed
* added monitoring system and NoSQLDB connection paramenters
* bug fix
* Some Certification tests
* Set default dryRun to True
* bug fixes for ProductionRequestDB
* Added Error code and corrected docstring
* Use LCG 85a
* Get sites from logbook
* Use eLog API instead of binary
* Added documentation for elog binary
* Extended email agent to post status changes to elog
* removed unused/untested production test infrastructure
* using proxy of connecting user for submitting production
* removed use of types
* RSS system test
* remove lbvobox24
* remove lbvobox23
* remove lbvobox13
* remove lbvobox12
* remove lbvobox16
* remove lbvobox32
* remove lbvobox07
* remove lbvobox22
* web server is updated differently
* Use External v6r4p2
* remove pre-version
* remove CMT reference for project
* MOve create_vobox_update from LHCbDiracPolicy to dist-tools
* change version of pre-release
* extract pyparsing
* Chnage the break statement by return to stop the execution if the SOAP interface is not accessible
* Break if the soap interface not accessible
* change version of pre-release

[v8r3p12]
use DIRAC v6r15p21

* Use GridCEType to be consistent with existing GridCE
* Use CommandExtensions and port 443
* not use file as variable name
* pilot.json filename and LHCb portal host name
* Import version with Setups and CEs sections
* Use lastRun
* keep in cache the actual RAW ancestors rather than count (due to common ancestors)
* New file with transf0rmation-debug actual code
* Move active methods to a Client file
* Do not consider a run if the number of files is not equal to the number of Unused files (_byRun())
* remove imports unused
* pylint happy

[v8r3p11]
use DIRAC v6r15p20
* add real data to the list in the correct place
* Update make_release.rst

[v8r3p10]
use DIRAC v6r15p19
* change the variable name
* remove " in docstring
* only consider local sites to SE
* add protection
* fix bkQueryProduction

[v8r3p9]
use DIRAC v6r15p19
* fix for histoMerging productions
* terminate directory printing with a /
* New script for executing command in a loop, just changing one substring
* remove from input files that are also in output (intermediate)

[v8r3p8]
use DIRAC v6r15p19
* implement new DQ procedure
* improve the dq flagging
* do not flag bad the Real data when the processing pass is given

[v8r3p7]
use DIRAC v6r15p18

* fix using lb-run
* allow better information formatting
* avoid exception if no memory
* set MJF power only if obtained from MJF, i.e. # DB12

[v8r3p6]
use DIRAC v6r15p18
* Fixes NagiosTopologyAgent
* Test scripts client_TransformationSystem.sh; dirac-test-production.py; random_files_creator.sh
* Use LHCbGrid v10r1

[v8r3p5]
use DIRAC v6r15p17
* change variable name
* Get list of real runs from range
* Allow to get run ranges for various metadata
* Fixes in NagiosTopologyAgent.py
* Fix case when the key is not a string
* New options to get runs from a transformation and display single item information grouped by run
* fix return value in case of error __getTransformationInfo
* Change switch --NoLFC to --NoFC

[v8r3p4]
use DIRAC v6r15p16

* added check for special case of histogram merging production

[v8r3p3]
use DIRAC v6r15p16
* Using Core/Security/Locations in __getProxy
* add availability in pfn-metadata --Exist
* remove @timeThis Avoid SEs not in FromSEs to be set in targetSE in _byRun()
* group the requests by role, requestname and working group
* fix get-sisters when more than one file from the same job is given as input
* allow more than one run number
* DMS.ScriptExecutor: normalize the lfn for dirac-dms-add-file
* clean up
* small fix
* Allow "wildcards" in BK path also for get-stats
* improve the documentation
* remove CMT based commands

[v8r3p2]
use DIRAC v6r15p13

* use v6r4p2 for Externals
* Added checks on consistency between types of real methods and the gateway service ones.
* Added working group to the subject
* Revert change
* Added a check for empty results
* Replaced notification client with diracAdmin
* Fixed database bug
* Added asterisks to init
* Added html flag and corrected a DB issue
* do not update the web server machine in the usual way.

[v8r3p1]
use DIRAC v6r15p13
* fixed doc
* Recover v8r2 PluginUtilities
* better error reporting
* Removed distinction about pre-release
* fix type mismatch
* allow to flag Real Data and a bit cleaning
* removed outdated
* from SetupProject to lb-run
* DataUsage: make the service backward compatible for sendDataUsageReport
* Install the web server differently in order to minimise the user failure.
* warn release manager to not merege devel to master by incident.
* add documentation about how to merge devel to master branches.
* remove lbvobox42

[v8r3]
use DIRAC v6r15p12

* better error reporting
* adding the file types
* added bkk client/system test
* fixes on testJobDefinitions.py
* Html notifications for ProductionStatusAgent
* Updated Utils.py according to PR 118
* using self-produced file
* added testprod script to sh file
* fixes RSS ConfigTemplate
* added test production creation script
* using setup for determining outConfig
* Grouping of status changes for _mailProdManager
* changed getMemberMails back to _getMemberMails
* added S_OK and changed global variable
* Added HTML support
* Basic aggregation
* Fixes NagiosTopologyAgent.py and included dryRun (Will Add DryRun on CS)
* fix: sendDataUsageReport expects at least 3 arguments
* added test for RootMerging productions
* unsetting BINARY_TAG to avoid confusion with lb-run projects
* Added rootMerging WF integration test
* bug fixes
* Fixes
* DataIntegrityClient using ConsistencyChecks methods
* fixes for interactions between ConsistencyInspector and DataIntegrity
* Added pilot commands for tests
* fix jenkins test
* Changes in LHCbDIRAC/tests/Utilities/testJobDefinitions.py Creates files on time and Delete them after with unique content and name:
* minor test fixes
* allow to restart service from CLI
* BKK: fix the catalog interface
* Popularity: add a popularity analysis agent
* replace fullpath by fullname for pyparsing
* job submission
* mods xml for regression tests
* removed LHCbCleanPilot command
* remove lbvobox80
* better code with more comments
* Change way of installing pyparsing
* fixes for MultiProcessor
* added LHCbCleanPilotEnv pilot command
* Changed from /bin/csh to /bin/bash Changed all the chs sintax to bash Added line to get user directory (e.g. /m/msoares) Changed all exit status from 0 to dirac script exit status Changed lhcb_shifter lhcb_prmgr (my bad, I used shifter for testing before my prmgr status was working)
* added MP tests
* fixed location of logs and XMLSummaries
* Change continue to Return Result if the SOAP interface is not accessible for VOMS
* client_test.py split into 2
* Change LCGCMT 84
* add scripts in .PHONY
* several fixes
* checking for ZeroDivisionError
* RAWIntegrity: displaced code in the DB
* os.mkdir to mkDir
* PitExport: add documentation
* RAWIntegrity: add test for the RAWIntegrityDB
* using mkDir utility
* RAWIntegrity: add showTables and removeFile methods
* RAWIntegrity: insert the initial value in LastMonitor when creating the DB
* Randomize the output string of the test jobs
* BKK: properly check the catalog arguments
* enabling updateRemotePilotFile service
* removed checkers and replaced str() with repr()
* fixes for submit and match test
* bug fix: analyseLogFile call
* added getTests()
* pylint
* remove gMonitor
* Revived HCCommand
* added a check for json
* updated documantation
* Revived Hammercloud Client
* v8r3-pre14 based on Dirac v6r15p2 and LHCbGrid v9r4
* Update create_vobox_update.py remove lbvobox10 and add lbvobox10X machines
* location
* pylint
* minor fixes for tests
* every time when the replica flag is updated we commit the transaction...
* use the new method for getting the produced events for a given production
* implemenet a new method which return the production producced events
* remove not used conditions
* Corect the URL for Client releases
* Add option for the CLIENT release
* bkk oracle docs
* Fix regression introduced by 8b4f6ffd95f1a11164b897941e454cdb163eb337
*  do not check the EventStat and FullStat
* use the view if the visibility flag is present
* removed unused script
* removed duplicated scripts
* pylint stuff
* really minor fixes to ProductionManagement modules
* added getCFGFile
* disable parallel queries. Change the error message to warn and do not change the dqvalue if we have an error...
* avoid navigating for some cases
* Make changes following very good review comments
* take into account the selected filetype when a list of steps returned from the db
* if the BkQuery does not exist, still set the BK input proc pass
* delete lb_run configuration in order to release LHCbDIRAC
* Allow replication shares to be using the run duration rather than the number of files
* MaxReset -> Problematic allowed
* when we use the visibility flag, we must join the productionscontainer with the bview in order to make sure that the correct execution plan is used
* Handling the case when XML summary file is not created
* more recognizable debug messages
* changed names of tests to standard structure
* Add option --DumpNotAtSE to dump LFNs not at a given (list of) SEs
* Use stderr for the progress bar. Allows to see if even if output is redirected to a file or a pipe
* We have to able to flag a run, before the register it to the bookkeeping. If the run is flagged, we use to flag the raw files using that flag
* add an utility package used to fix wrong information in the db
* Went from basestring to str. Basesting not working with v6r14.
* Fixed WMSSecureGW Service
* LHCbCreatePilotcfg has been replaced with the LHCbCleanPilotEnv command.
* Updated BOINC gateway service
* New configuration service that will update the LHCb-Pilot.json file when a change is done on the CS.

[v8r2p56]
Use DIRAC v6r14p39
* flag RAW files and all file in a processing pass and descendants
* fix script to flag RAW files + processing pass and accept "/Real Data" as processing pass to start from
* better names than input and output
* fix bug when getting run duration from TS
* fix type of GroupSize
* If input files are RAW, don't get RAW ancestors
* Consider files in BK and not in FC when checking descendants
* report descendants in Failover
* accept "," as jobID separators
* Improve change of input file status in case of error
* Get core dump info from log
* New script for registering a file from BK and SE informations
* New method to register in the FC a file registered in the BK and existing in SE
* Improve result printout
* Add options to get only input or output
* Do not cache the number of RAW files between calls to the plugin

[v8r2p55]
Use DIRAC v6r14p39
* fixes for name of variable
* allowing custom list of modules for Gaudi steps
* fix for case of non-existing ProDetail

[v8r2p54]
Use DIRAC v6r14p39
* treating special case of merging root/histo/ntuple
* Transformation plugins for root merging productions
* Tests for Histo Merging production
* add example about how to create a JIRA ticket for deployment
* improve the release procedure
* Add the location where the LHCbGrid version can be found.
* add the certification client installation procedure

[v8r2p53]
Use DIRAC v6r14p38
* to many arguments to format the string.

[v8r2p52]
Use DIRAC v6r14p38

* Nagios Topology
* use the correct name
* add the working group and the request name to the email subject
* use the new URL for downloading the oracle client

[v8r2p51]
Use DIRAC v6r14p38

* setting Unused files back to unused using request
* inicialize only once the class variables
* restored use of simple lists
* added scaffolding for using DAGs
* avoid variable called type
* Mostly better documentation
* created Mock_BookkeepingClient
* split pro duction Client tests
* minor fixes, mostly backported
* remove not used import

[v8r2p50]
Use DIRAC v6r14p37
* using Tier1-DST and resolveSEGroup
* no plugin necessary if ListProcessingPass
* Simplify getBKProductions
* tuple is a valid iterable for runIDs which must be integers
* allow to configure an email address using CS
* use pretty printing from BK
* allow to only give prefix of WN name (useful for BOINC)
* fix test of argument of getRunsMetadata
* make scaleSize public
* Track users who does not provide enough parameters
* make sure the file type is not a list
* moved interactions with CS in initialize
* If the file type is raw, we only match jobs which production is less than 0

[v8r2p49]
Use DIRAC v6r14p36

* bulk update implemented
* bulk event type insert is implemented
* flag to get TCK of runs without FULL stream fix printout
* add option to only list processing passes with wildcard
* new script to set the run destination based on majority of derived files
* Revert back to not forcing input files to be reset Unused when Processe

[v8r2p48]
Use DIRAC v6r14p34
* add comment
* update docstrings
* Use with statement
* docstring
* Implement reviewer's comments
* catch exception
	* New script for listing TCKs used by a run range
* Make a function for getting run ranges (in order to reuse)
	* allow to flag multiple processing passes
	* Add information about git
	* Fix exception when EventStat is None
	* recreate res from success and failed
* remove useless .keys()
	* simpler
	* move body of several BK scripts (those with quite some logic) into a ScriptExecutors module
	* New module with body of BK scripts
	* replace LFC with FC
	* replace LFC with FC
	* revert
	* use iterators and generators
	* use iterators
	* use iterators
	* Protect empty runs
	* use generators and iterator

	[v8r2p47]
	Use DIRAC v6r14p31
	* no need to eval
	* added choice of visibility flag

	[v8r2p46]
	Use DIRAC v6r14p31
	* TCK added to the getFilesSummary method
	* TCK added to the getFilesWithMetadata method

	[v8r2p45]
	Use DIRAC v6r14p31
	* Allow nb of Raw files to evolve
	* Avoid timeouts when publishing
	* add "Finished" flag
	* The view is not  used when the file type is RAW
	* create a file every time when the command is executed
	* add the script which updates the voboxes

	[v8r2p44]
	Use DIRAC v6r14p31
	* remove static methods of the BKK XMLFilesReaderManager.py 


	[v8r2p43]
	Use DIRAC v6r14p31
	* improve application status
	* fix exception logging
	* oups, bk returns ['Failed'] as a list, not a dict...
	* cut file list in chunks
	* Small improvement in --Info flush printout
	* run number can be a long :(
			* the return value of the getFileMetadata has changed, becauase of that we have to use successful dictionary

			[v8r2p42]
			User DIRAC v6r14p30
			* force status to be updated to Unused
			* Remove double return value in bk.getFileMetadata()
			* remove obsolete plugin
			* fix a bug and remove obsolete plugins
			* fixes in logging
			* use file info if replicaFlag specified
			* change chunksize
			* Add option --IgnoreFileType (if only interested in processing pass)
			* every time when the replica flag is updated we commit the transaction...
			* use the new method for getting the produced events for a given production
			* implemenet a new method which return the production producced events
			* remove not used conditions
			* Corect the URL for Client releases
			* Add option for the CLIENT release

			[v8r2p41]
	use DIRAC v6r14p28
	* bkk oracle docs
	* Fix regression introduced by 8b4f6ffd95f1a11164b897941e454cdb163eb337
	* do not check the EventStat and FullStat
	* use the view if the visibility flag is present

	[v8r2p40]
	use DIRAC v6r14p26
	* avoid useless cast
	* take into account the selected filetype when a list of steps returned from the db
	* if the BkQuery does not exist, still set the BK input proc pass
	* Allow replication shares to be using the run duration rather than the number of files
	* MaxReset -> Problematic allowed

	[v8r2p39]
	use DIRAC v6r14p26
	* delete lb_run configuration in order to release LHCbDIRAC
	* when we use the visibility flag, we must join the productionscontainer with the bview in order to make sure that the correct execution plan is used
	* Add option --DumpNotAtSE to dump LFNs not at a given (list of) SEs
	* Use stderr for the progress bar. Allows to see if even if output is redirected to a file or a pipe
	* We have to able to flag a run, before the register it to the bookkeeping. If the run is flagged, we use to flag the raw files using that flag
	* improve the release procedure
	* add an utility package used to fix wrong information in the db
	* Fixed WMSSecureGW Service
	* LHCbCreatePilotcfg has been replaced with the LHCbCleanPilotEnv command.
	* Modified LHCbCreatePilotcfg command
	* Updated BOINC gateway service
	* New configuration service that will update the LHCb-Pilot.json file when a change is done on the CS.

	[v8r2p38]
	use DIRAC v6r14p25
	* Change URL for general doc of DIRAC
	* add the command to really update the pilot version
	* Better doc for releases
	* if GSI exist do try to re-create the link
	* add more information about the client installation
	* Using input events only if present, else outputEv
	* recording input events in BK XML summary
	* just pylint types of fixes
	* Change location for tools and create scripts directory
	* add projectConfig.json
	* Change location of manifest
	* new generation of manifest and LHCbDirac.xenv
	* add Gen_GSI and gen_xenv

	[v8r2p37]
	* Use DIRAC v6r14p25
	* Change parsing of template information.
	* use the view for building the sim/datataking descriptions
	* take into account the visibility flag...
	* set the flag to recompute the directory size an option in Operations
	* Add a 1 second sleep in case of failure to add files in TS
	* Fix addBookkeepingQueryRunList() when runs exist
	* Add protection against removed files
	* Replace gLogger.always() with notice() Add some logic for FC2SE in order to take into account banned SEs
	* Use ScriptExecutors methods for removing files/replicas Replace gLogger.always() with gLogger.notice() as advised for scripts
	* Replace DIRAC.exit() with return <statusCode>
	* add DIRAC exit code from execute method
	* Add new instruction sto make the LHCbDirac distribution
	* Don't stop if error
	* @%@%## of bug in the DFC, one call was not fixed properly
	* When the view is used, we have to join to the table
	* Added initial doc on Jenkins
	* FIX: Various fixes to the release guide for prod

	[v8r2p36]
	* Use Dirac v6r14p24
	* Make a look on eventTypes rather than a query with a list, it is faster
	* Add dock string
	* adding to release notes
	* Added release guide
	* Add changelog
	* For some reason the dirac-production-runjobloca script was sent to gitlab in an ond version
	* use the bookkeeping view
	* Skip RAW data but for /LHCb
	* Temporary fix for recalculating the directory size
	* Fix doc string and comments
* Add a new method for checking a file was processed (inactive)
	* Fix DQFlag as name is # in TS and BK
	* Protect agains Conditions == None
* Fix DQFlag -> DataqualityFlag as metadata

[v8r2p35]
* CHANGE: use DIRAC v6r14p22
* FIX: removed dirac-platform script from LHCbDIRAC
