
LHCbDIRAC is the LHCb extension of [DIRAC](https://github.com/DIRACGrid/DIRAC).

Important links
===============

- Official source code repo: https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC
- HTML documentation (stable release): http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/lhcbdirac/
- Issue tracker: https://its.cern.ch/jira/browse/LHCBDIRAC/
- Developers Mailing list: https://groups.cern.ch/group/lhcb-dirac/default.aspx
- KB articles about GitLab usage: https://cern.service-now.com/service-portal/topic.do?topic=Gitlab&s=it

Install
=======

For more detailed installation instructions, see the DIRAC documentation.

