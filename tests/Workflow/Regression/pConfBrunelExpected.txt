from ProdConf import ProdConf

ProdConf(
  NOfEvents=2,
  DDDBTag='dddb-20130929',
  CondDBTag='sim-20130522-vc-md100',
  AppVersion='v43r2p7',
  XMLSummaryFile='summaryBrunel_00012345_00006789_4.xml',
  Application='Brunel',
  OutputFilePrefix='00012345_00006789_4',
  XMLFileCatalog='pool_xml_catalog.xml',
  InputFiles=['LFN:00012345_00006789_3.digi'],
  OutputFileTypes=['dst'],
)